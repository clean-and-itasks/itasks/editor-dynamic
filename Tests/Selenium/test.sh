#!/bin/bash

set -e

NODE_PID=""
cleanup () {
	kill $NODE_PID 2>/dev/null
}

trap 'ps -p $! >/dev/null || exit 1' CHLD
trap cleanup INT TERM

node --unhandled-rejections=strict ./nitrile-packages/linux-x64/itasks/test/selenium/driver.js &
NODE_PID="$!"
sleep 5

test-runner -r ./Tests/Selenium/Test/iTasks.UI.Editor.Controls --options '--headless;--window-size;1920x1080;--screenshots-for-failed-tests' --junit junit-selenium-controls.xml

cleanup
