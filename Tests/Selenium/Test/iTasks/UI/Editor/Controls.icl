module Test.iTasks.UI.Editor.Controls

import StdEnv
import Data.Func
import Text
import iTasks, iTasks.Testing.Selenium, iTasks.Testing.Selenium.Interface
import iTasks.Extensions.Editors.DynamicEditor

Start w = runTestSuiteWithCLI tests w

TEST_1_NAME :== "Using dynamic dropdown editor does not lead to JS error because width attribute is set to null"

tests :: [TestedTask]
tests =
	[ TestedTask (dynamicEditorDropdownTask <<@ Name TEST_1_NAME) dynamicEditorDropdownTest
	]

:: Expr = Add Expr Expr | Mul Expr Expr | Val Int
derive class iTask Expr

dynamicEditorDropdownTask :: Task ()
dynamicEditorDropdownTask = enterInformation [EnterUsing id (dynamicEditor dynExprEditor)] @! ()
where
	dynExprEditor :: DynamicEditor Expr
	dynExprEditor =
		DynamicEditor
			[ DynamicCons $ functionCons "add" "Add" (\e1 e2 -> Add e1 e2)
			, DynamicCons $ functionCons "mul" "Mul" (\e1 e2 -> Mul e1 e2)
			// Not necessary to make this a functionConsDyn of course.
			, DynamicCons $ functionConsDyn "val" "Val" (dynamic \i -> Val i :: Int -> Expr)
			, DynamicCons $ customEditorCons "intVal" "Enter integer" intEditor
			]
	where
		intEditor :: Editor Int (EditorReport Int)
		intEditor = gEditor {|*|} EditValue

dynamicEditorDropdownTest url =
	seleniumTest TEST_1_NAME $
	getURL url `then` \_ ->
	findElement (ByTestName TEST_1_NAME) NoElement `then` \task ->
	waitForIdle task $
	findElement (ByCSS "select") NoElement `cont`
	resolvePromise ()
