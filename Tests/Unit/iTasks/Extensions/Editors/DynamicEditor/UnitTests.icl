module iTasks.Extensions.Editors.DynamicEditor.UnitTests

import StdEnv
import Data.Func, Data.Either, Data.Functor
import Text.GenPrint
import iTasks, iTasks.Extensions.Editors.DynamicEditor, iTasks.Testing, iTasks.Testing.Unit, iTasks.Internal.TaskIO

// TODO: The derives should not be done in this module.
derive gPrint TaskOutputMessage, UIChange, UIChildChange, UIAttributeChange, UI, JSONNode, Map, UIType

/**
 * The `updateInformation` task is initialised with a fixed value,
 * the custom child editor `customEditor` changes the value to the provided `value`.
 * The task should provide the changed value and not the original one.
 */
providesValueChangedByChild :: !(?String) -> UnitTest
providesValueChangedByChild value =
	testTaskResult ("provides value changed by child editor for " +++ printToString value) task events value checkEqual
where
	task :: Task (?String)
	task =
		updateInformation
			[UpdateUsing id (\_ v -> v) $ dynamicEditor dynEditor]
			{constructorId = "ID", value = DEJSONValue $ toJSON "should be discarded"}
		@? tvToMaybe
		@ fmap (fromOk o valueCorrespondingTo dynEditor)
		>>~ return

	dynEditor = DynamicEditor [DynamicCons $ customEditorCons "ID" "custom editor" customEditor]

	customEditor :: Editor String (EditorReport String)
	customEditor = mapEditorWrite (\_ -> maybe EmptyEditor ValidEditor value) $ gEditor{|*|} EditValue

	events = [Left ResetEvent]

tests =
	[ providesValueChangedByChild ?None
	, providesValueChangedByChild $ ?Just ""
	, providesValueChangedByChild $ ?Just "some string"
	]

Start world = runUnitTests tests world
