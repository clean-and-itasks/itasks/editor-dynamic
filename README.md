# itask-dynamic-editor

This provides dynamic editors, which can be constructed dynamically by data
and also can make use of the power of the dynamic type system which allows for quantified type variables.
This makes it possible to achieve type safety similar to the safety provided by GADTs.

The main idea is to provide a number of dynamic conses producing values and requiring arguments.
For all required arguments the user has the choice between all dynamic conses providing a value of the proper type.

## Licence

The package is licensed under the BSD-2-Clause license; for details, see the
[LICENSE](/LICENSE) file.
