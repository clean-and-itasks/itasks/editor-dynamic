# Changelog

#### 0.13.3

- Enhancement: improved standard layout to visually indicate nesting of constructors.

#### 0.13.2

- Feature: reuse children with matching type

#### 0.13.1

- Chore: update iTasks dependency to 0.16.0.

## 0.13.0

- Change: add `DynamicConsId -> Bool` parameter to listConsDynWithOptions,
          which allows to filter the children of a dynamic list editor based on constructor id.
          When updating, to get the same behavior as before, use `\_ = True`.
- Feature: add `listConsDynWithPredOnChildConses`.

#### 0.12.6

- Chore: accept `base` `3.0`.

#### 0.12.5

- Chore: support `iTasks 0.15`.

#### 0.12.4

- Feature: add `parametrisedDynamicEditorWithMissingConses` and `htmlCorrespondingToWithMissingConses`.
           These functions can be used to provide custom behavior if dynamic editor values constructed
           using a dynamic editor contain references to constructor ids which are not part of the dynamic editor.
           This can be useful if the constructors which are part of the dynamic editor depend on external data
           (e.g from a database) and there are references to constructor ids which do not exist for a
           dynamic editor value, given the state of the external data.

#### 0.12.3

- Chore: adapt to `iTasks 0.14`.

#### 0.12.2

- Feature: add `==` instance for `:: DynamicEditorValue`.

#### 0.12.1

- Chore: support iTasks 0.13 alongside iTasks 0.12.

## 0.12.0

- Initial version, import modules from clean platform v0.3.38 and destill all
  generic hashing modules.
