implementation module iTasks.Extensions.Editors.DynamicEditor

import Control.GenBimap
from Control.Monad import foldM, mapM
import Data.Func, Data.Functor, Data.GenHash
from Data.List import intersperse, zip4, zipWith
import qualified Data.Map as Map
from Data.Map import derive gEq Map
import Data.Tuple
import Gast => qualified label
import Gast.Gen
import StdEnv
import Text, Text.GenPrint, Text.HTML

import iTasks
import iTasks.UI.Editor.Common

derive genShow JSONNode
derive gPrint JSONNode
derive ggen JSONNode

dynamicEditor :: !(DynamicEditor a) -> Editor (DynamicEditorValue a) (EditorReport (DynamicEditorValue a)) | TC a
dynamicEditor dynEditor = dynamicEditor` True dynEditor

dynamicEditor` :: !Bool !(DynamicEditor a) -> Editor (DynamicEditorValue a) (EditorReport (DynamicEditorValue a)) | TC a
dynamicEditor` checkForDuplicates dynEditor =
	compoundEditorToEditor $ dynamicCompoundEditor False checkForDuplicates dynEditor

dynamicEditorWithChildPred :: !(DynamicConsId -> Bool) !Bool !(DynamicEditor a) -> Editor (DynamicEditorValue a) (EditorReport (DynamicEditorValue a)) | TC a
dynamicEditorWithChildPred childPred checkForDuplicates dynEditor =
	compoundEditorToEditor $ dynamicCompoundEditorWithChildPred childPred False checkForDuplicates dynEditor

parametrisedDynamicEditorWithMissingConses ::
	!(p -> DynamicEditor a) -> Editor (!p, !?(DynamicEditorValue a)) (EditorReport (DynamicEditorValue a))
	| TC a & gEq{|*|}, TC p
parametrisedDynamicEditorWithMissingConses editor = parametrisedDynamicEditor` True True editor

parametrisedDynamicEditor ::
	!(p -> DynamicEditor a) -> Editor (!p, !?(DynamicEditorValue a)) (EditorReport (DynamicEditorValue a))
	| TC a & gEq{|*|}, TC p
parametrisedDynamicEditor editor = parametrisedDynamicEditor` False True editor

parametrisedDynamicEditor` ::
	!Bool !Bool !(p -> DynamicEditor a) -> Editor (!p, !?(DynamicEditorValue a)) (EditorReport (DynamicEditorValue a))
	| TC a & gEq{|*|}, TC p
parametrisedDynamicEditor` okIfUnknown checkForDuplicates editor =
	compoundEditorToEditor
		{ CompoundEditor
		| onReset = onReset editor, onEdit = onEdit editor, onRefresh = onRefresh editor, writeValue = writeValue editor
		}
where
	onReset ::
		!(p -> DynamicEditor a) !UIAttributes !(?(p, ?(DynamicEditorValue a))) !*VSt
		->
			( !MaybeErrorString (UI, (p, ?(DynamicConsId, ConsType, Bool)), [EditState], !(?(EditorReport (DynamicEditorValue a))))
			, !*VSt
			)
		| TC a
	onReset editor attr mbval vst
		= case mbval of
			?None
				= abort "Enter mode not supported by parametrisedDynamicEditor.\n"
			?Just (p, val)
				= appFst
					(fmap \(ui,st,cst,mbw) -> (ui,(p,st),cst,mbw))
					((dynamicCompoundEditor okIfUnknown checkForDuplicates $ editor p).CompoundEditor.onReset
						attr val vst)

	onEdit ::
		!(p -> DynamicEditor a) !(!EditorId, !JSONNode) !(!p, !?(DynamicConsId, ConsType, Bool)) ![EditState] !*VSt
		->
			( !MaybeErrorString
				(?(UIChange, (p, ?(DynamicConsId, ConsType, Bool)), [EditState], !(?(EditorReport (DynamicEditorValue a)))))
			, *VSt
			)
		| TC a
	onEdit editor event (p, mbSt) childSts vst
		= appFst
			(fmap (fmap \(change,st,cst,mbw) -> (change,(p,st),cst,mbw)))
			((dynamicCompoundEditor okIfUnknown checkForDuplicates $ editor p).CompoundEditor.onEdit
				event mbSt childSts vst)

	onRefresh ::
		!(p -> DynamicEditor a) !(?(!p, !?(DynamicEditorValue a))) !(!p, !?(DynamicConsId, ConsType, Bool))
		![EditState] !*VSt
		->
			( !MaybeErrorString
				(UIChange, (p, ?(DynamicConsId, ConsType, Bool)), [EditState], !(?(EditorReport (DynamicEditorValue a))))
			, !*VSt
			)
		| TC a
	onRefresh editor ?None st childSts vst
		= abort "Enter mode not supported by parametrisedDynamicEditor.\n"
	onRefresh editor (?Just (p, new)) st=:(p`, mbSt) childSts vst =
		case underlyingEditorOldParam.CompoundEditor.writeValue mbSt childSts of
			// If the editor has no valid value and there is no new value provided,
			// we cannot update it yet without loosing the partial value.
			// TODO: this be solved with a proper `onRefresh` for the ordinary dyn editor, see #338
			Ok (InvalidEditor _) | isNone new
				= (Ok (NoChange, st, childSts, ?None), vst)
			Ok EmptyEditor = case new of
				?Just new = appFst
					(fmap \(change,st,cst,mbw) -> (change,(p,st),cst,mbw))
					(underlyingEditorNewParam.CompoundEditor.onRefresh (?Just new) mbSt childSts vst)
				?None =
					(Ok (NoChange, st, childSts, ?None), vst)

			Ok (ValidEditor old)
				# value = fromMaybe old new
				# (uiForOldP, vst) = underlyingEditorOldParam.CompoundEditor.onReset 'Map'.newMap (?Just value) vst
				| isError uiForOldP = (liftError uiForOldP, vst)
				# (uiForOldP, _, _, _) = fromOk uiForOldP
				# (uiForNewP, vst) = underlyingEditorNewParam.CompoundEditor.onReset 'Map'.newMap (?Just value) vst
				| isError uiForNewP = (liftError uiForNewP, vst)
				# (uiForNewP, newSt, newChildSts, newMbW) = fromOk uiForNewP
				//Because the generated UI's each have a fresh unique editorId generated during onReset
				//We must ignore all 'editorId' attributes during the comparison
				| isEqualExceptEditorId uiForOldP uiForNewP =
					// If the definition of the editor itself remains unchanged,
					// a refresh on the underlying editor is only required if a new value is provided.
					case new of
						?Just new =
							appFst
								(fmap \(change,st,cst,mbw) -> (change,(p,st),cst,mbw))
								(underlyingEditorNewParam.CompoundEditor.onRefresh (?Just new) mbSt childSts vst)
						?None =
							(Ok (NoChange, st, childSts, ?None), vst)
				| otherwise =
					(Ok (ReplaceUI uiForNewP, (p, newSt), newChildSts, newMbW), vst)
			Error e
				= (Error e, vst)
	where
		underlyingEditorOldParam = dynamicCompoundEditor okIfUnknown False $ editor p`
		underlyingEditorNewParam = dynamicCompoundEditor okIfUnknown False $ editor p

	writeValue ::
		!(p -> DynamicEditor a) !(!p, !?(DynamicConsId, ConsType, Bool)) ![EditState]
		-> MaybeErrorString (EditorReport (DynamicEditorValue a)) | TC a
	writeValue editor (p,st) childSts = (dynamicCompoundEditor okIfUnknown False $ editor p).CompoundEditor.writeValue
		st childSts

	isEqualExceptEditorId :: !UI !UI -> Bool
	isEqualExceptEditorId (UI t1 a1 i1) (UI t2 a2 i2)
		= t1 === t2
		&& 'Map'.del "editorId" a1 === 'Map'.del "editorId" a2
		&& length i1 == length i2
		&& (and $ zipWith isEqualExceptEditorId i1 i2)

// Bool part of result indicates whether the type is correct, i.e. the child types are matching
dynamicCompoundEditor :: !Bool !Bool !(DynamicEditor a) -> CompoundEditor
	(?(!DynamicConsId, !ConsType, !Bool)) (DynamicEditorValue a) (EditorReport (DynamicEditorValue a)) | TC a
dynamicCompoundEditor okIfUnknown checkForDuplicates dynEditor =
	dynamicCompoundEditorWithChildPred (\_ = True) okIfUnknown checkForDuplicates dynEditor

// Bool part of result indicates whether the type is correct, i.e. the child types are matching
dynamicCompoundEditorWithChildPred :: !(DynamicConsId -> Bool) !Bool !Bool !(DynamicEditor a) -> CompoundEditor
	(?(!DynamicConsId, !ConsType, !Bool)) (DynamicEditorValue a) (EditorReport (DynamicEditorValue a)) | TC a
dynamicCompoundEditorWithChildPred childPred okIfUnknown checkForDuplicates dynEditor=:(DynamicEditor elements)
	| checkForDuplicates && duplicateIds
		= abort $ concat ["duplicate cons IDs in dynamic editor: ", printToString duplicateIds, "\n"]
	# {DynamicChildEditor|onReset, onEdit, onRefresh, writeValue} = dynamicChildEditor okIfUnknown dynEditor
	= {CompoundEditor|onReset = onReset childPred
	  , onEdit = onEdit childPred, onRefresh = onRefresh childPred, writeValue = writeValue
	  }
where
	duplicateIds = duplicateIds` $ sort $ (\(b, _) -> b.consId) <$> consesOf elements
	where
		duplicateIds` :: ![DynamicConsId] -> Bool
		duplicateIds` [x: rest =: [y: _]]
			| x == y = True
			| otherwise     = duplicateIds` rest
		duplicateIds` _ = False

dynamicChildEditor ::
	!Bool !(DynamicEditor a)
	-> DynamicChildEditor
			(?(DynamicConsId, ConsType, Bool)) (DynamicEditorValue a) (EditorReport (DynamicEditorValue a))
	| TC a
dynamicChildEditor okIfUnknownOnReset dynEditor=:(DynamicEditor elements)
	= {DynamicChildEditor| onReset = onReset
	  , onEdit = onEdit, onRefresh = onRefresh, writeValue = writeValue
	  }
where
	// conses with optional group labels
	conses = consesOf elements

	onReset ::
		!(DynamicConsId -> Bool) !UIAttributes !(?(DynamicEditorValue a)) !*VSt
		-> *(!MaybeErrorString
				(!UI, !?(!DynamicConsId, !ConsType, !Bool)
					, ![EditState], ?(EditorReport (DynamicEditorValue a))), !*VSt)
	onReset pred attr mbval vst=:{VSt|taskId}
		# (editorId,vst) = nextEditorId vst
		# matchingConsesWPred = matchingConses pred
		= case mbval of
		?None = case matchingConsesWPred of
			[(onlyChoice, _)] | (hideCons matchingConsesWPred)
				# (mb, vst) = resetChildEditors matchingConsesWPred onlyChoice.consId ?None vst
				| isError mb = (liftError mb, vst)
				# (uis, childSts, _, type, _) = fromOk mb
				# attrs = 'Map'.union (withContainerClassAttr onlyChoice.uiAttributes) attr
				= ( Ok
						( uiContainer False attrs uis
						, ?Just (onlyChoice.consId, type, True)
						, [nullState editorId: childSts]
						, ?Just EmptyEditor
						)
					, vst
					)
			_ = case filter (\(cons, _) -> cons.useAsDefault) (matchingConsesWPred) of
				[(defaultChoice, _): _]
					# (mb, vst) = resetChildEditors matchingConsesWPred defaultChoice.consId ?None vst
					| isError mb = (liftError mb, vst)
					# (uis, childSts, idx, type, label) = fromOk mb
					# attrs = 'Map'.union (withContainerClassAttr defaultChoice.uiAttributes) attr
					| hideCons matchingConsesWPred =
						( Ok (uiContainer False attrs uis
							, ?Just (defaultChoice.consId, type, True)
							, [nullState editorId: childSts]
							, ?Just $ maybe EmptyEditor ValidEditor mbval
							)
						, vst
						)
					| otherwise
						# (consChooseUI, chooseSt) = genConsChooseUI matchingConsesWPred taskId editorId (?Just idx)
						= ( Ok ( uiContainer True attrs [consChooseUI: uis]
							   , ?Just (defaultChoice.consId, type, True)
							   , [chooseSt: childSts]
							   , ?Just $ maybe EmptyEditor ValidEditor mbval
							   )
						  , vst
						  )
				_
					# (consChooseUI, chooseSt) = genConsChooseUI matchingConsesWPred taskId editorId ?None
					= (Ok (uiContainer True attr [consChooseUI], ?None, [chooseSt], ?None), vst)

		?Just {constructorId, value}
			# (mb, vst) = resetChildEditors matchingConsesWPred constructorId (?Just value) vst
			| isError mb = (liftError mb, vst)
			# (uis, childSts, idx, type, label) = fromOk mb
			# mbcons = consWithId okIfUnknownOnReset constructorId matchingConsesWPred
			| isError mbcons = (liftError mbcons, vst)
			# (cons, _) = fromOk mbcons
			# attrs = 'Map'.union (withContainerClassAttr cons.uiAttributes) attr
			# hideC = hideCons matchingConsesWPred
			# (uis, childSts) =
				if hideC
					(uis, [nullState editorId: childSts])
					( let (consChooseUI, chooseSt) = genConsChooseUI matchingConsesWPred taskId editorId (?Just idx)
					  in  ([consChooseUI: uis], [chooseSt: childSts])
					)
			# st  = ?Just (constructorId, type, True)
			| cons.consId == UNKNOWN_CONS_ID = (Ok (uiContainer (not hideC) attrs uis, ?None, childSts, ?None), vst)
			# val = writeValue st childSts
			| isError val = (liftError val, vst)
			= (Ok (uiContainer (not hideC) attrs uis, st, childSts, ?Just $ fromOk val), vst)

	genConsChooseUI :: ![(DynamicCons, ?String)] !String !EditorId !(?Int) -> (!UI, !EditState)
	genConsChooseUI matchingConsesWPred taskId editorId mbSelectedCons = (consChooseUI, consChooseSt)
	where
		consOptions =
			[ JSONObject $
					[("id", JSONInt i), ("text", JSONString cons.DynamicCons.label)]
				++
					maybe [] (\label -> [("grouplabel", JSONString label)]) mbGroupLabel
			\\ (cons, mbGroupLabel) <- matchingConsesWPred & i <- [0..]
			]
		consChooseUI =
			uia
				UIDropdown (choiceAttrs taskId editorId (maybe [] (\x -> [x]) mbSelectedCons) consOptions)
		consChooseSt = LeafState {editorId= ?Just editorId,touched=False,state= ?Just (dynamic mbSelectedCons)}

	resetChildEditors ::
		![(DynamicCons, ?String)] !DynamicConsId !(?DEVal) !*VSt
		-> *(!MaybeErrorString (![UI], ![EditState], Int, ConsType, String), !*VSt)
	resetChildEditors matchingConsesWPred cid mbval vst
		# mbcons = consWithId okIfUnknownOnReset cid matchingConsesWPred
		| isError mbcons = (liftError mbcons, vst)
		# (cons=:{childPred}, idx) = fromOk mbcons
		# type = case cons.builder of
			FunctionCons     _ = Function
			ListCons     _ _ _ = List
			CustomEditorCons _ = CustomEditor
		= case cons.builder of
			FunctionCons fbuilder
				# children =
					reverse $ zip4 vals (childrenEditors fbuilder) (cons.DynamicCons.labels ++ repeat ?None) [0..]
				# (mbUis, vst) = resetChildEditors` children [] [] vst
				| isError mbUis = (liftError mbUis, vst)
				# (accUi, accSt) = fromOk mbUis
				= (Ok (accUi, accSt, idx, type, cons.DynamicCons.label), vst)
			where
				resetChildEditors` ::
					![(?(String, DEVal), DCE, ?String, Int)] ![UI] ![EditState] !*VSt
					-> (!MaybeErrorString ([UI], [EditState]), !*VSt)
				resetChildEditors` [] accUi accSt vst = (Ok (accUi, accSt), vst)
				resetChildEditors` [(mbVal, DCE dynChildEditor, mbLabel, i): children] accUi accSt vst
					# {Editor|onReset} = dynamicChildEditorToEditor (childPred i) dynChildEditor
					# mbVal = (\(cid, val) -> {constructorId = cid, value = val}) <$> mbVal
					= case onReset 'Map'.newMap mbVal vst of
						(Ok (ui, st, _), vst)
							= resetChildEditors` children [withLabel mbLabel ui: accUi] [st:accSt] vst
						(Error e,     vst) = (Error e, vst)
				where
					withLabel :: !(?String) !UI -> UI
					withLabel (?Just label) (UI type attrs item) = UI type ('Map'.union attrs $ labelAttr label) item
					withLabel ?None         ui                   = ui

				vals = case mbval of
					// update mode
					?Just (DEApplication children) = [?Just (cid,val) \\ (cid, val) <- children]
					// enter mode
					_                              = repeat ?None
			ListCons lbuilder removable reorderable
				# listEditorMode = (\(DEApplication listElems) -> listElems) <$> mbval
				# (mbUi, vst)
					= (listBuilderEditor removable reorderable childPred lbuilder).Editor.onReset 'Map'.newMap listEditorMode vst
				| isError mbUi = (liftError mbUi, vst)
				# (ui, st, _) = fromOk mbUi
				= (Ok ([ui], [st], idx, type, cons.DynamicCons.label), vst)
			CustomEditorCons editor
				# editorVal =
					(\(DEJSONValue json) -> fromMaybe (abort "Invalid dynamic editor state\n") $ fromJSON json) <$> mbval
				# (mbUi, vst) = editor.Editor.onReset 'Map'.newMap editorVal vst
				| isError mbUi = (liftError mbUi, vst)
				# (ui, st, _) = fromOk mbUi
				= (Ok ([ui], [st], idx, type, cons.DynamicCons.label), vst)

	onEdit ::
		!(DynamicConsId -> Bool)
		!(!EditorId, !JSONNode)
		!(?(!DynamicConsId, !ConsType, !Bool))
		![EditState]
		!*VSt
		-> *(!MaybeErrorString (? (!UIChange, !?(!DynamicConsId, !ConsType, !Bool)
				, ![EditState], ?(EditorReport (DynamicEditorValue a)))) , !*VSt)
	// new builder is selected: create a UI for the new builder
	onEdit pred (eventId, JSONArray [JSONInt builderIdx]) st
		[LeafState {LeafState|editorId= ?Just editorId}: childrenSts] vst | editorId == eventId
		# matchingConsesWPred = matchingConses pred
		| builderIdx < 0 || builderIdx >= length matchingConsesWPred
			= (Error "Dynamic editor selection out of bounds", vst)
		# (cons, _) = matchingConsesWPred !! builderIdx
		# (mb, vst) = resetChildEditors matchingConsesWPred cons.consId ?None vst
		| isError mb = (liftError mb, vst)
		# (uis, childSts, _, type, _) = fromOk mb
		// insert new UIs for arguments
		# matchesPrevCons = maybe False (\justState = consBuilderCompare cons.builder (consForState justState matchingConsesWPred)) st
		# inserts = if matchesPrevCons [] [(i, InsertChild ui) \\ ui <- uis & i <- [1..]]
		# removals = if matchesPrevCons [] (removeNChildren $ length childrenSts)
		// add "itasks-container" classes as this class always has to be present for containers
		# uiAttrs = withContainerClassAttr cons.uiAttributes
		# attrChange  = if (typeWasInvalid st) removeErrorIconAttrChange []
		# childChange =
				if (typeWasInvalid st) removeErrorIconChange []
			++
				[	( 0
					, ChangeChild $
						ChangeUI (uncurry SetAttribute <$> 'Map'.toList uiAttrs) (removals ++ inserts)
					)
				]
		# builderChooseState = LeafState {editorId = ?Just editorId, touched = True, state = ?Just (dynamic (length uis))}
		# change             = ChangeUI attrChange childChange
		# state              = ?Just (cons.consId, type, True)
		# childStates        =  if matchesPrevCons [builderChooseState: childrenSts] [builderChooseState: childSts]
		# value              = writeValue state childStates
		| value =: (Error _) = (liftError value,vst)
		= (Ok (?Just (change, state, childStates, ?Just (fromOk value))), vst)

	// other events targeted directly at this cons
	onEdit pred (eventId,e) st [LeafState {LeafState|editorId= ?Just editorId}: childSts] vst | eventId == editorId
		| e =: JSONNull || e =: (JSONArray []) // A null or an empty array are accepted as a reset events
			//If necessary remove the fields of the previously selected cons
			# attrChange  = if (typeWasInvalid st) removeErrorIconAttrChange []
			# childChange =
					if (typeWasInvalid st) removeErrorIconChange []
				++
					[(0, ChangeChild $ ChangeUI [] $ removeNChildren $ length childSts)]
			= (Ok (?Just (ChangeUI attrChange childChange, ?None, [nullState editorId], ?Just EmptyEditor)), vst)
		| otherwise
			= (Error $ concat ["Unknown dynamic editor select event: '", toString e, "'"], vst)

	// update is potentially targeted somewhere inside this value
	onEdit pred (eventId, e) (?Just (cid, type, typeWasCorrect)) childSts vst
		# matchingConsesWPred = matchingConses pred
		# mbcons = consWithId False cid matchingConsesWPred
		| isError mbcons = (liftError mbcons, vst)
		# (cons, _) = fromOk mbcons
		# (res, vst) = case cons.builder of
			FunctionCons fbuilder
				# children = childrenEditors fbuilder
				= onEditFunctionCons cons.childPred 0 children (eventId,e) (tl childSts) vst
			ListCons lbuilder removable reorderable
				= case
					(listBuilderEditor
						removable reorderable cons.childPred lbuilder).Editor.onEdit (eventId, e) (childSts !! 1) vst
				  of
					(Ok ?None,vst) = (Ok ?None,vst)
					(Ok (?Just (change,state,mbw)),vst) = (Ok (?Just (0,change,state)),vst)
					(Error e,vst) = (Error e,vst)
			CustomEditorCons editor
				= case editor.Editor.onEdit (eventId, e) (childSts !! 1) vst of
					(Ok ?None,vst) = (Ok ?None,vst)
					(Ok (?Just (change,state,mbw)),vst) = (Ok (?Just (0,change,state)),vst)
					(Error e,vst) = (Error e,vst)
		= case res of
			Ok ?None = (Ok ?None,vst)
			Ok (?Just (argIdx, change, childSt))
				# childChange =
					[	(0
						, ChangeChild
							$ ChangeUI [] [(argIdx + if (hideCons matchingConsesWPred) 0 1, ChangeChild change)]
						)
					]
				# change      = ChangeUI mbErrorIconAttrChange $ childChange ++ mbErrorIconChange
				// replace state for this child
				# state = ?Just (cid, type, isOk typeIsCorrect)
				# value = writeValue state childSts`
				| value =: (Error _) = (liftError value, vst)
				= (Ok (?Just (change, state, childSts`,?Just (fromOk value))), vst)
			where
				childSts`   = updateAt (argIdx + 1) childSt childSts
				typeIsCorrect = childTypesAreMatching cons.builder (drop 1 childSts`)

				(mbErrorIconChange, mbErrorIconAttrChange) = mbErrorIconUpd
				mbErrorIconUpd
					| typeWasCorrect && isError typeIsCorrect
						# classes =
							JSONString <$> ["itasks-container", "itasks-horizontal", "itasks-dynamic-editor-error"]
						=	( [(1, InsertChild errorIcon)]
							, [SetAttribute "class" $ JSONArray classes]
							)
					with
						errorIcon =
							UI
								UIContainer
								('Map'.singleton "class" $ JSONString "itasks-dynamic-editor-icon-error-container")
								[ UI
									UIIcon
									('Map'.union (iconClsAttr "icon-invalid") (tooltipAttr $ fromError typeIsCorrect))
									[]
								]
					| not typeWasCorrect && isOk typeIsCorrect =
						(removeErrorIconChange, removeErrorIconAttrChange)
					| otherwise = ([], [])

			Error e = (Error e, vst)
		where
			onEditFunctionCons childPred i [] _ _ vst
				= (Ok ?None,vst)
			onEditFunctionCons childPred i [(DCE dynChildEditor):cs] (eventId,e) [st:states] vst
				# {Editor|onEdit} = dynamicChildEditorToEditor (childPred i) dynChildEditor
				= case onEdit (eventId,e) st vst of
					(Error e,vst) = (Error e,vst)
					(Ok (?Just (change,st,_)),vst) = (Ok (?Just (i,change,st)),vst)
					(Ok ?None,vst) = onEditFunctionCons childPred (i+1) cs (eventId,e) states vst

	onEdit _ _ _ _ vst = (Ok ?None, vst)

	typeWasInvalid :: !(?(!DynamicConsId, !ConsType, !Bool)) -> Bool
	typeWasInvalid (?Just (_, _, False)) = True
	typeWasInvalid _                     = False

	removeErrorIconChange     = [(1, RemoveChild)]
	removeErrorIconAttrChange =
		[SetAttribute "class" $ JSONArray [JSONString "itasks-container", JSONString "itasks-horizontal"]]

	// add "itasks-container" classes as this class always has to be present for containers
	withContainerClassAttr :: !(Map String JSONNode) -> Map String JSONNode
	withContainerClassAttr attrs = 'Map'.alter (?Just o addContainerClass) "class" attrs
	where
		addContainerClass :: !(?JSONNode) -> JSONNode
		addContainerClass mbJSONClasses = JSONArray [JSONString "itasks-container": otherClasses]
		where
			otherClasses = maybe [] (\(JSONArray classes) -> classes) mbJSONClasses

	removeNChildren :: !Int -> [(Int, UIChildChange)]
	removeNChildren nrArgs = repeatn nrArgs (1, RemoveChild)

	// build a builder according to the state
	consForState :: !(!DynamicConsId, !ConsType, !Bool) ![(DynamicCons,?String)]  -> DynamicConsBuilder
	consForState (consId, _, _) matching = hd [cons.builder\\ (cons, _) <- matching | cons.consId == consId]

	consBuilderCompare :: !DynamicConsBuilder !DynamicConsBuilder -> Bool
	consBuilderCompare (FunctionCons a) (FunctionCons b) = haveMatchingTypes a b
	consBuilderCompare (ListCons a _ _ ) (ListCons b _ _) = haveMatchingTypes a b
	consBuilderCompare _ _ = False

	haveMatchingTypes :: !Dynamic !Dynamic -> Bool
	haveMatchingTypes a b = case (a,b) of
		(a :: z, _ :: z) = True
		_ = False

	onRefresh ::
		!(DynamicConsId -> Bool)
		!(?(DynamicEditorValue a))
		!(?(!DynamicConsId, !ConsType, !Bool))
		![EditState]
		!*VSt
		-> *(!MaybeErrorString (!UIChange, !(?(!DynamicConsId, !ConsType, !Bool)), ![EditState], ?(EditorReport (DynamicEditorValue a))), !*VSt)
	// TODO: how to get UI attributes?
	// TODO: fine-grained replacement
	onRefresh _ ?None st [LeafState {LeafState|editorId= ?Just editorId}: childSts] vst //Soft reset
		//If necessary remove the fields of the previously selected cons
		# attrChange  = if (typeWasInvalid st) removeErrorIconAttrChange []
		//Reset the dropdown
		# selectorChanges = [(0, ChangeChild (ChangeUI [SetAttribute "value" (JSONArray [JSONInt -1,JSONBool True])] []))]
		# childChange =
				if (typeWasInvalid st) removeErrorIconChange []
			++
				[(0, ChangeChild $ ChangeUI [] (selectorChanges ++ (removeNChildren $ length childSts)))]
		= (Ok (ChangeUI attrChange childChange, ?None, [nullState editorId], ?None), vst)

    onRefresh pred (?Just new) st childSts vst
		# value = writeValue st childSts
		| value =: (Error _) = (liftError value, vst)
		| isNotChanged (fromOk value) new = (Ok (NoChange, st, childSts, ?None), vst)
		# (res,vst) = onReset pred 'Map'.newMap (?Just new) vst
		= (fmap (\(ui,st,csts,mbw) -> (ReplaceUI ui, st, csts, mbw)) res, vst)
	where
		isNotChanged (ValidEditor {constructorId, value}) {constructorId = consId`, value = val`} =
			constructorId == consId` && value === val`
		isNotChanged _ _ =
			False
	hideCons matchingConsesWPred = case matchingConsesWPred of
		[(onlyChoice, _)] | not onlyChoice.showIfOnlyChoice = True
		_                                                   = False

	matchingConses pred = catMaybes $
		(\(cons, mbGroupLabel) -> (\cons` -> (cons`, mbGroupLabel)) <$> matchingCons pred dynEditor cons) <$> conses
	
	// first arg only used for type
	// packs matching conses, with possibly updated (= more specific) type
	matchingCons :: !(DynamicConsId -> Bool) !(DynamicEditor a) !DynamicCons -> ?DynamicCons | TC a
	matchingCons pred dynEd cons=:{builder, consId}
		| pred consId = (\b -> {cons & builder = b}) <$> mbBuilder`
		= ?None
	where
		mbBuilder` = case builder of
			FunctionCons     fbuilder                       = matchf fbuilder
			CustomEditorCons editor                         = matchc editor
			ListCons         lbuilder removable reorderable = matchl lbuilder removable reorderable

		// works for functions with upto 10 args
		// the type of the dynamic is updated by unifying the function result with the type produced by the editor
		matchf :: !Dynamic -> ?DynamicConsBuilder
		matchf b = case (b, dynamic dynEd) of
			(b :: a b c d e f g h i j -> z, _ :: DynamicEditor z) = ?Just $ FunctionCons (dynamic b)
			(b :: a b c d e f g h i   -> z, _ :: DynamicEditor z) = ?Just $ FunctionCons (dynamic b)
			(b :: a b c d e f g h     -> z, _ :: DynamicEditor z) = ?Just $ FunctionCons (dynamic b)
			(b :: a b c d e f g       -> z, _ :: DynamicEditor z) = ?Just $ FunctionCons (dynamic b)
			(b :: a b c d e f         -> z, _ :: DynamicEditor z) = ?Just $ FunctionCons (dynamic b)
			(b :: a b c d e           -> z, _ :: DynamicEditor z) = ?Just $ FunctionCons (dynamic b)
			(b :: a b c d             -> z, _ :: DynamicEditor z) = ?Just $ FunctionCons (dynamic b)
			(b :: a b c               -> z, _ :: DynamicEditor z) = ?Just $ FunctionCons (dynamic b)
			(b :: a b                 -> z, _ :: DynamicEditor z) = ?Just $ FunctionCons (dynamic b)
			(b :: a                   -> z, _ :: DynamicEditor z) = ?Just $ FunctionCons (dynamic b)
			(b ::                        z, _ :: DynamicEditor z) = ?Just $ FunctionCons (dynamic b)
			_                                                     = ?None

		// custom editors do not allow for quantified variables, so no type update is required
		matchc e = case (dynamic e, dynamic dynEd) of
			(_ :: Editor a (EditorReport a), _ :: DynamicEditor a) = ?Just $ CustomEditorCons e
			_                                          = ?None

		matchl f removable reorderable = case (f, dynamic dynEd) of
			(f :: (a -> b, [b] -> c), _ :: DynamicEditor c) = ?Just $ ListCons (dynamic f) removable reorderable
			_                                               = ?None

	listBuilderEditor :: !Bool !Bool !(Int DynamicConsId -> Bool) !Dynamic -> Editor [(DynamicConsId, DEVal)] [EditorReport (DynamicConsId, DEVal)]
	listBuilderEditor removable reorderable childPred ((mapF, _) :: (a -> b, [b] -> c))
		= listEditor False (?Just $ const ?None) removable reorderable ?None ValidEditor childrenEd`
	where
		childrenEd  = childrenEditorList mapF
		childrenEd`
			= mapEditorRead	(\(cid, val) -> {constructorId = cid, value= val})
			$ mapEditorWrite (fmap (\{constructorId, value} -> (constructorId, value)))
			$ childrenEd

		// first argument only used for type
		childrenEditorList :: (a -> b) -> Editor (DynamicEditorValue a) (EditorReport (DynamicEditorValue a)) | TC a
		childrenEditorList _ = dynamicEditorWithChildPred (childPred 0) False (DynamicEditor elements)

	listBuilderEditor _ _ _ _ = abort "dynamic editors: invalid list builder value\n"

	uiContainer :: !Bool !UIAttributes ![UI] -> UI
	uiContainer withChoice attr uis =
		UI
			UIContainer
			('Map'.singleton "class" $ JSONArray
				[ JSONString "itasks-container", JSONString "itasks-wrap-height", JSONString "itasks-horizontal"
				: if withChoice [JSONString "itasks-dynamic-editor-container-with-choice"] []])
			[UI UIContainer attr uis]

	writeValue ::
		!(?(!DynamicConsId, !ConsType, !Bool)) ![EditState] -> *MaybeErrorString (EditorReport (DynamicEditorValue a))
	writeValue (?Just (cid, CustomEditor, True)) [_: [editorSt]]
		# mbcons = consWithId False cid conses
		| isError mbcons = liftError mbcons
		# ({builder}, _) = fromOk mbcons
		// toJSON` is used to solve overloading, JSONEncode{|*|} is attached to CustomEditorCons
		# (editor, toJSON`) = case builder of
			CustomEditorCons editor = (editor, toJSON)
			_                       = abort "corrupt dynamic editor state\n"
		= case editor.Editor.writeValue editorSt of
			Ok mbvalue = Ok $ (\val -> {constructorId = cid, value = DEJSONValue $ toJSON` val}) <$> mbvalue
			Error e = Error e

	writeValue (?Just (cid, type, True)) [_: childSts] = case childValuesFor childSts` [] of
		Ok childVals = Ok $ (\vals -> {constructorId = cid, value = DEApplication vals}) <$> childVals
		Error e = Error e
	where
		childSts` = case (type, childSts) of
			(List, [CompoundState _ childSts]) = childSts
			(_,    childSts)                   = childSts

		childValuesFor :: ![EditState] ![(DynamicConsId, DEVal)] -> MaybeErrorString (EditorReport [(DynamicConsId, DEVal)])
		childValuesFor [] acc = Ok $ ValidEditor $ reverse acc
		childValuesFor [childSt: childSts] acc = case (dynamicEditor` False dynEditor).Editor.writeValue childSt of
			Ok (ValidEditor {constructorId = childCid, value = childVal}) =
				childValuesFor childSts [(childCid, childVal): acc]
			Ok (InvalidEditor reasons) = Ok (InvalidEditor reasons)
			Ok EmptyEditor = Ok EmptyEditor
			Error e = Error e

	writeValue _ _ = Ok EmptyEditor

	childrenEditors :: !Dynamic -> [DCE]
	childrenEditors (f :: a -> b) = [DCE $ dynamicEditorFstArg f : childrenEditors (dynamic (f undef))]
	where
		// first argument only used for type
		dynamicEditorFstArg ::
			(a -> b) -> DynamicChildEditor (?(String, ConsType, Bool)) (DynamicEditorValue a) (EditorReport (DynamicEditorValue a)) | TC a
		dynamicEditorFstArg _
			= dynamicChildEditor okIfUnknownOnReset $ DynamicEditor elements

	childrenEditors _         = []

	childTypesAreMatching :: !DynamicConsBuilder [EditState] -> MaybeErrorString ()
	childTypesAreMatching (FunctionCons cons) childStates =
		childTypesAreMatching` cons (childValueOf <$> zip2 childStates (childrenEditors cons))
	where
		childTypesAreMatching` :: !Dynamic ![?Dynamic] -> MaybeErrorString ()
		childTypesAreMatching` _ [] = Ok ()
		childTypesAreMatching` cons [?None: otherArgs] =
			case cons of
				(cons` :: a -> z) = childTypesAreMatching` (dynamic cons` undef) otherArgs
		childTypesAreMatching` cons [?Just nextArg: otherArgs] =
			case (cons, nextArg) of
				// `cons` undef` has type z`, which is z updated by unifying the type of the next arg
				(cons` :: a -> z, _ :: a) = childTypesAreMatching` (dynamic cons` undef) otherArgs
				_                         =
					Error $
						concat
							[ "Could not unify\n    ", toString (argOf $ typeCodeOfDynamic cons), "\nwith\n    "
							, toString (typeCodeOfDynamic nextArg)
							]

		childValueOf :: !(!EditState, !DCE) -> ?Dynamic
		childValueOf (state, DCE dynamicChildEditor)
			// writeValue does not use the childPred so it can be const True.
			# {Editor|writeValue} = dynamicChildEditorToEditor (const True) dynamicChildEditor
			= case writeValue state of
				Ok (ValidEditor value) = error2mb $ valueCorrespondingToDyn (DynamicEditor elements) value
				_ = ?None

		argOf :: !TypeCode -> TypeCode
		argOf (TypeApp (TypeApp _ arg) _) = arg
		argOf (TypeScheme _ type)         = argOf type
	// only function conses can have not matching child types
	childTypesAreMatching _ _ = Ok ()

dynamicChildEditorToCompoundEditor
	:: !(DynamicConsId -> Bool) !(DynamicChildEditor st r w) -> CompoundEditor st r w | TC st
dynamicChildEditorToCompoundEditor childPred {DynamicChildEditor|onReset, onEdit, onRefresh, writeValue} =
	{CompoundEditor|onReset = onReset childPred
	, onEdit = onEdit childPred, onRefresh = onRefresh childPred, writeValue = writeValue
	}

dynamicChildEditorToEditor :: !(DynamicConsId -> Bool) !(DynamicChildEditor st r w) -> Editor r w | TC st
dynamicChildEditorToEditor childPred dynChildEditor =
	compoundEditorToEditor $ dynamicChildEditorToCompoundEditor childPred dynChildEditor

valueCorrespondingTo :: !(DynamicEditor a) !(DynamicEditorValue a) -> MaybeErrorString a | TC a
valueCorrespondingTo dynEditor dynEditorValue = case valueCorrespondingToDyn dynEditor dynEditorValue of
	Ok (v :: a^) = Ok v
	Ok val = Error $ "Unexpected type: " +++ toString (typeCodeOfDynamic val)
	Error s = Error s

stringCorrespondingToOkIfUnknown :: !(DynamicEditor a) !(DynamicEditorValue a) -> MaybeErrorString String
stringCorrespondingToOkIfUnknown editor value = stringCorrespondingTo` True editor value

stringCorrespondingTo :: !(DynamicEditor a) !(DynamicEditorValue a) -> MaybeErrorString String
stringCorrespondingTo editor value = stringCorrespondingTo` False editor value

stringCorrespondingTo` :: !Bool !(DynamicEditor a) !(DynamicEditorValue a) -> MaybeErrorString String
stringCorrespondingTo` okIfUnknown (DynamicEditor elements) {constructorId, value}
	# mbStr = stringCorrespondingTo` (constructorId, value) []
	| isError mbStr = liftError mbStr
	# str = fromOk mbStr
	= Ok o concat $ withCapitalisedFirstLetter $
		dropWhile (\s -> textSize (trim s) == 0) $ reverse [".": str]
where
	withCapitalisedFirstLetter :: ![String] -> [String]
	withCapitalisedFirstLetter [firstString: rest] = [upperCaseFirst firstString: rest]

	stringCorrespondingTo` :: (!DynamicConsId, !DEVal) ![String] -> MaybeErrorString [String]
	stringCorrespondingTo` (cid, val) accum
		# mbcons = consWithId okIfUnknown cid $ consesOf elements
		| isError mbcons = liftError mbcons
		# (cons, _) = fromOk mbcons
		= case val of
			DEApplication args = case cons.builder of
				FunctionCons fbuilder =
					foldM (flip stringCorrespondingTo`) [" ", cons.DynamicCons.label : accum] args
				ListCons _ _ _
					# mbStrs = mapM (\arg -> stringCorrespondingTo` arg []) $ reverse args
					| isError mbStrs = liftError mbStrs
					# strs = fromOk mbStrs
					# listElStrs = flatten $ intersperse [" ", cons.DynamicCons.label] strs
					= Ok $ listElStrs ++ [" "] ++ accum
				_ = Error "corrupt dynamic editor value"
			DEJSONValue json = case cons.builder of
				CustomEditorCons editor = Ok [ " ", stringCorrespondingToGen editor json
										  , " ", cons.DynamicCons.label
										  : accum
										  ]
				_ = Error "corrupt dynamic editor value"

stringCorrespondingToGen :: (Editor a (EditorReport a)) !JSONNode -> String | gText{|*|}, JSONDecode{|*|}  a
stringCorrespondingToGen editor json = toSingleLineText $ fromJSON` editor json
where
	fromJSON` :: (Editor a (EditorReport a)) !JSONNode -> a | JSONDecode{|*|} a
	fromJSON` _ json =
		fromMaybe (abort $ concat ["corrupt dynamic editor value ", toString json, "\n"]) $ fromJSON json

htmlCorrespondingToWithMissingConses ::
	!(DynamicConsId -> HtmlTag) !(DynamicEditor a) !(DynamicEditorValue a) -> MaybeErrorString HtmlTag
htmlCorrespondingToWithMissingConses unknownConstructorToHtml editor value =
	htmlCorrespondingTo` (?Just unknownConstructorToHtml) editor value

htmlCorrespondingTo :: !(DynamicEditor a) !(DynamicEditorValue a) -> MaybeErrorString HtmlTag
htmlCorrespondingTo editor value = htmlCorrespondingTo` ?None editor value

htmlCorrespondingTo` :: !(?(DynamicConsId -> HtmlTag)) !(DynamicEditor a) !(DynamicEditorValue a) -> MaybeErrorString HtmlTag
htmlCorrespondingTo` htmlIfUnknown (DynamicEditor elements) {constructorId, value}
	# mbhtml = toHtml (constructorId, value)
	| isError mbhtml = mbhtml
	# html = fromOk mbhtml
	= Ok $ DivTag [ClassAttr "itasks-dynamic-editor-description"] [html]
where
	conses = consesOf elements

	toHtml :: !(!DynamicConsId, !DEVal) -> MaybeErrorString HtmlTag
	toHtml (cid, val)
		# mbcons = consWithId False cid conses
		| isError mbcons = maybe (liftError mbcons) (\htmlIfUnknown = Ok $ htmlIfUnknown cid) htmlIfUnknown
		# ({builder, label, uiAttributes}, _) = fromOk mbcons
		# cssClasses = case 'Map'.get "class" uiAttributes of
			?Just (JSONArray cs) -> [c \\ JSONString c <- cs]
			_                    -> []
		# classAttr = ClassAttr $ join " " ["itasks-container":cssClasses]
		= case val of
			DEApplication args
				# args = mapM toHtml args
				| isError args = liftError args
				# args = fromOk args
				= case builder of
					FunctionCons fbuilder ->
						Ok $ DivTag [classAttr]
							[ SpanTag [] [Text label]
							: args
							]
					ListCons _ _ _ ->
						Ok $
							DivTag [classAttr] $
								// Label for the list editor.
								[ SpanTag [StyleAttr "margin-left:0;word-break:normal;"] [Text label]
								// Items within the list editor.
								, UlTag [StyleAttr "margin-top:0;"] $
									map (\arg -> DivTag [StyleAttr "display:flex"] [arg]) args
								]
					_ ->
						Error "corrupt dynamic editor value"
			DEJSONValue json = case builder of
				CustomEditorCons editor ->
					Ok $ DivTag [classAttr]
						[ SpanTag [] [Text $
							if (size label == 0)
								(stringCorrespondingToGen editor json)
								(concat3 label ": " (stringCorrespondingToGen editor json))]
						]
				_ ->
					Error "corrupt dynamic editor value"

// It is boxed anyway (because it's using abstract types)
:: DynamicCons = !
	{ consId           :: !DynamicConsId
	, label            :: !String
	, builder          :: !DynamicConsBuilder
	, showIfOnlyChoice :: !Bool
	, useAsDefault     :: !Bool
	, uiAttributes     :: !UIAttributes
	, labels           :: ![?String]
	, childPred        :: !(Int DynamicConsId -> Bool)
	}

:: DynamicConsBuilder
	=      FunctionCons     !Dynamic
	| E.a: CustomEditorCons !(Editor a (EditorReport a)) & JSONEncode{|*|}, JSONDecode{|*|}, gText{|*|}, TC a
	|      ListCons         !Dynamic !Bool !Bool
		//* dynamic must contain a value of type (a -> b, [b] -> c)
		//* First bool indicates whether elements are removable, second Bool indicates whether elements are reorderable.

functionCons :: !DynamicConsId !String !a -> DynamicCons | TC a
functionCons consId label func = functionConsDyn consId label (dynamic func)

functionConsDyn :: !DynamicConsId !String !Dynamic -> DynamicCons
functionConsDyn consId label func = functionConsDynWithPredOnChildConses consId label (\_ _ -> True) func

functionConsDynWithPredOnChildConses :: !DynamicConsId !String !(Int DynamicConsId -> Bool) !Dynamic -> DynamicCons
functionConsDynWithPredOnChildConses consId label childPred func =
	{ consId           = consId
	, label            = label
	, builder          = FunctionCons func
	, showIfOnlyChoice = True
	, useAsDefault     = False
	, uiAttributes     = 'Map'.newMap
	, labels           = []
	, childPred        = childPred
	}

listCons :: !DynamicConsId !String !([a] -> b) -> DynamicCons | TC a & TC b
listCons consId label func = listConsDyn consId label (dynamic (id, func) :: (a^ -> a^, [a^] -> b^))

listConsDyn :: !DynamicConsId !String !Dynamic -> DynamicCons
listConsDyn consId label func = listConsDynWithOptions True True consId label (\_ = True) func

listConsDynWithPredOnChildConses :: !DynamicConsId !String !(DynamicConsId -> Bool) !Dynamic -> DynamicCons
listConsDynWithPredOnChildConses consId label childPred func =
	listConsDynWithOptions True True consId label childPred func

listConsDynWithOptions :: !Bool !Bool !DynamicConsId !String !(DynamicConsId -> Bool) !Dynamic -> DynamicCons
listConsDynWithOptions elementsRemovable elementsReorderable consId label childPred func =
	{ consId           = consId
	, label            = label
	, builder          = ListCons func elementsRemovable elementsReorderable
	, showIfOnlyChoice = True
	, useAsDefault     = False
	, uiAttributes     = 'Map'.newMap
	, labels           = []
	, childPred        = (\_ = childPred)
	}

customEditorCons ::
	!DynamicConsId !String !(Editor a (EditorReport a)) -> DynamicCons | TC, JSONEncode{|*|}, JSONDecode{|*|}, gText{|*|} a
customEditorCons consId label editor =
	{ consId           = consId
	, label            = label
	, builder          = CustomEditorCons editor
	, showIfOnlyChoice = True
	, useAsDefault     = False
	, uiAttributes     = 'Map'.newMap
	, labels           = []
	, childPred        = \_ _ -> True
	}

instance tune DynamicConsOption DynamicCons where
	tune :: !DynamicConsOption !DynamicCons -> DynamicCons
	tune HideIfOnlyChoice          cons = {cons & showIfOnlyChoice = False}
	tune UseAsDefault              cons = {cons & useAsDefault = True}
	tune (ApplyCssClasses classes) cons = {cons & uiAttributes = 'Map'.union (classAttr classes) cons.uiAttributes}
	tune (AddLabels labels)        cons = {DynamicCons| cons & labels = labels}

valueCorrespondingToDyn :: !(DynamicEditor a) !(DynamicEditorValue a) -> MaybeErrorString Dynamic | TC a
valueCorrespondingToDyn (DynamicEditor elements) {constructorId, value} = valueCorrespondingTo` (constructorId, value)
where
	valueCorrespondingTo` :: !(!DynamicConsId, !DEVal) -> MaybeErrorString Dynamic
	valueCorrespondingTo` (cid, val)
	# mbcons = (\(cons, _) -> cons) <$> (consWithId False cid $ consesOf elements)
	| isError mbcons = liftError mbcons
	# cons = fromOk mbcons
	= case val of
		DEApplication args = case cons.builder of
			FunctionCons fbuilder     = valueCorrespondingToFunc fbuilder args
			ListCons     lbuilder _ _ = valueCorrespondingToList lbuilder args
			_                         = Error "corrupt dynamic editor value"
		DEJSONValue json = case cons.builder of
			CustomEditorCons editor = valueCorrespondingToGen editor json
			_                       = Error "corrupt dynamic editor value"

	valueCorrespondingToFunc :: !Dynamic ![(DynamicConsId, DEVal)] -> MaybeErrorString Dynamic
	valueCorrespondingToFunc v [] = Ok v
	valueCorrespondingToFunc f [x=:(consId, _) : xs] = case dynValue of
		Ok dynValue -> case (f, dynValue) of
			(f :: a -> b, x :: a) = valueCorrespondingToFunc (dynamic (f x)) xs
			_ =
				Error $
					concat
						[ "Cannot unify demanded type with offered type for constructor '", toString consId, "':\n "
						, firstArgString $ typeCodeOfDynamic f, "\n ", toString $ typeCodeOfDynamic dynValue, "\n"
						]
		e -> liftError e
	where
		dynValue = valueCorrespondingTo` x

		firstArgString :: !TypeCode -> String
		firstArgString (TypeScheme _ tc)              = firstArgString tc
		firstArgString (TypeApp (TypeApp _ fstArg) _) = toString fstArg
		firstArgString _                              = "no argument required"

	valueCorrespondingToGen :: (Editor a (EditorReport a)) !JSONNode -> MaybeErrorString Dynamic | JSONDecode{|*|}, TC a
	valueCorrespondingToGen editor json = Ok (dynamic (fromJSON` editor json))
	where
		fromJSON` :: (Editor a (EditorReport a)) !JSONNode -> a | JSONDecode{|*|} a
		fromJSON` _ json =
			fromMaybe (abort $ corruptValueErrorString ["undecodable JSON ", toString json, "\n"]) $ fromJSON json

	valueCorrespondingToList :: !Dynamic ![(DynamicConsId, DEVal)] -> MaybeErrorString Dynamic
	valueCorrespondingToList funcs args =
		case [mappedArgument $ valueCorrespondingTo` val \\ val <- args] of
			[]
				-> case funcs of
					((_, g) :: (a -> b, [b] -> c)) = Ok (dynamic g [])
					_ = Error "corrupt dynamic editor value"
			// We use the first element to update the type,
			// an arbitrary element can be used as the `b` and `c` type variable
			// is required to be equal for all list elements.
			args=:[fst: _] -> case fst of
					Ok fst -> case (funcs, fst) of
						((f, g) :: (x, [b] -> c), _ :: b) -> Ok (dynamic (g $ fromJust $ fromDynList args))
						_                                 -> Error "corrupt dynamic editor value"
					e -> liftError e
	where
		mappedArgument (Ok (val :: a))
			= case funcs of
				((f, _) :: (a^ -> b, x)) -> Ok (dynamic f val)
				_                        -> Error "corrupt dynamic editor value"
		mappedArgument err = liftError err

		fromDynList :: ![MaybeErrorString Dynamic] -> ?[b] | TC b
		fromDynList dyns = fromDynList` dyns []
		where
			fromDynList` []           acc = ?Just $ reverse acc
			fromDynList` [Ok (val :: b^): dyns] acc = fromDynList` dyns [val: acc]
			fromDynList` [Error _: dyns] acc = fromDynList` dyns acc

:: E = E.a: E (Editor (DynamicEditorValue a) (EditorReport (DynamicEditorValue a))) & TC a
:: ConsType = Function | List | CustomEditor

// This type is required to be able to pass a predicate used for filtering out child constructors
// to a dynamic editor constructor, see `functionConsDynWithPredOnChildConses`.
:: DynamicChildEditor st r w =
	{ onReset :: !(DynamicConsId -> Bool) UIAttributes (?r) *VSt ->
		*(MaybeErrorString (!UI, !st, ![EditState], !?w), *VSt) //* Generating the initial UI
	, onEdit :: !(DynamicConsId -> Bool) (!EditorId, !JSONNode) st [EditState] *VSt ->
		*(MaybeErrorString (?(!UIChange, !st, ![EditState], !?w)), *VSt) //* React to edit events
	, onRefresh :: !(DynamicConsId -> Bool) (?r) st [EditState] *VSt ->
		*(MaybeErrorString (!UIChange, !st, ![EditState], !?w), *VSt) //* React to a new model value
	, writeValue :: !st [EditState] -> MaybeErrorString w //...
		//* Compute the editor's write value from the editor state
	}

// Used for storing dynamic child editors with different states/read/write types in single list.
:: DCE = E.a st: DCE !(DynamicChildEditor st (DynamicEditorValue a) (EditorReport (DynamicEditorValue a))) & TC a & TC st

consWithId :: !Bool !DynamicConsId ![(DynamicCons, ?String)] -> MaybeErrorString (!DynamicCons, !Int)
consWithId okIfUnknown cid conses = case filter (\(({consId}, _), _) -> consId == cid) $ zip2 conses [0..] of
	[((cons, _), idx)] = Ok (cons, idx)
	[]                 = if okIfUnknown (Ok (unknownCons, -1)) (Error $ "dynamic editor: cons not found: '" +++ cid)
	_                  = Error $ "dynamic editor: duplicate conses: '" +++ cid

UNKNOWN_CONS_ID :== "_UNKNOWN"

/**
 * An unknown constructor, this may be used if a reference to a constructor within a dynamic editor value
 * could not be found.
 */
unknownCons :: DynamicCons
unknownCons =:
	{consId = UNKNOWN_CONS_ID, label = "UNKNOWN", builder = FunctionCons (dynamic ()), showIfOnlyChoice = True
	, useAsDefault = True, uiAttributes = 'Map'.newMap, labels = [], childPred = \_ _ = False}

nullState :: !EditorId -> EditState
nullState editorId = LeafState {editorId = ?Just editorId, touched = True, state = ?None}

consesOf :: ![DynamicEditorElement] -> [(DynamicCons, ?String)]
consesOf elements = flatten $ consesOf <$> elements
where
	consesOf :: !DynamicEditorElement -> [(DynamicCons, ?String)]
	consesOf (DynamicCons cons)              = [(cons, ?None)]
	consesOf (DynamicConsGroup label conses) = (\cons -> (cons, ?Just label)) <$> conses

corruptValueErrorString :: ![String] -> String
corruptValueErrorString errorStrs = concat $ flatten [["Corrupt dynamic editor value: "], errorStrs, [".\n"]]

dynamicEditorValueContainsConsId :: !(DynamicEditorValue a) !DynamicConsId -> Bool
dynamicEditorValueContainsConsId {constructorId, value} consId
	| constructorId == consId = True
	= dEvalContainsConsId value consId
where
	dEvalContainsConsId :: !DEVal !DynamicConsId -> Bool
	dEvalContainsConsId (DEApplication dEVals) consId =
		any (\(consIdOfVal, dEval) -> consId == consIdOfVal || dEvalContainsConsId dEval consId) dEVals
	dEvalContainsConsId (DEJSONValue _) _ = False

instance == (DynamicEditorValue a) where
	(==) d0 d1 = d0.constructorId == d1.constructorId && d0.DynamicEditorValue.value == d1.DynamicEditorValue.value

instance == DEVal derive gEq

derive class iTask DynamicEditorValue, DEVal
derive gPrint DynamicEditorValue, DEVal
derive gHash DynamicEditorValue, DEVal
derive genShow DynamicEditorValue, DEVal
derive JSONEncode ConsType
derive JSONDecode ConsType